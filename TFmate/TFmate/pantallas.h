#ifndef PANTALLAS_H
#define PANTALLAS_H

#include "upc.h"
using uint = unsigned int;

const uint HEIGHT = 28;
const uint WIDTH = 120;

void printxy(uint x, int y, std::string txt) {
    gotoxy(x, y);
    std::cout << txt;
}

void gotoxyprint(int x, int y, std::string line) {
    gotoxy(x, y);
    std::cout << line;
}

void printFrame(uint x, uint y, uint width, uint height) {
    foreground(WHITE);
    background(BLACK);
    gotoxy(x, y);
    std::cout << '\xDA';
    for (uint i = 0; i < width; i++) std::cout << '\xC4';
    std::cout << '\xBF';
    for (uint i = 1; i < height; i++) {
        gotoxy(x, y + i);
        std::cout << '\xB3';
        gotoxy(x + width + 1, y + i);
        std::cout << '\xB3';
    }
    gotoxy(x, y + height);
    std::cout << '\xC0';
    for (uint i = 0; i < width; i++) std::cout << '\xC4';
    std::cout << '\xD9';
}

void drawMiniMap(uint x, uint y, uint width, uint height) {
    background(BACKGROUND_BLUE);
    printFrame(x, y, width, height);
}

void drawBuffer(const char buffer[HEIGHT][WIDTH]) {
    for (uint y = 0; y < HEIGHT; ++y) {
        gotoxy(0, y);
        for (uint x = 0; x < WIDTH; ++x) {
            std::cout << buffer[y][x];
        }
    }
}

void generateSpaceMap(char buffer[HEIGHT][WIDTH]) {
    for (uint y = 0; y < HEIGHT; ++y) {
        for (uint x = 0; x < WIDTH; ++x) {
            buffer[y][x] = ' ';
        }
    }
}

void mostrarIntroduccion() {
    clear();
    printxy(5, 5, "Bienvenido al juego de prueba.");
    printxy(5, 7, "Presione cualquier tecla para continuar...");
    _getch();
}

int menuPrincipal(uint left, uint top, uint width, uint height) {
    clear();
    printFrame(left, top, width, height);
    foreground(WHITE);
    background(BACKGROUND_BLUE);
    gotoxy(left + 2, top + 1);
    std::cout << "MENU PRINCIPAL";
    gotoxy(left + 2, top + 3);
    std::cout << "1. Jugar";
    gotoxy(left + 2, top + 4);
    std::cout << "2. Reglas del Juego";
    gotoxy(left + 2, top + 5);
    std::cout << "3. Salir";
    gotoxy(left + 2, top + 7);
    std::cout << "Elija una opcion: ";
    return readIntRange("", 1, 3);
}

void reglasDelJuego() {
    clear();
    printxy(5, 5, "Estas son las reglas del juego:");
    printxy(5, 7, "1. Regla 1");
    printxy(5, 8, "2. Regla 2");
    printxy(5, 9, "3. Regla 3");
    printxy(5, 11, "Presione cualquier tecla para volver al menu...");
    _getch();
}

void jugar() {
    clear();
    printxy(5, 5, "�Que comience el juego!");
    printxy(5, 7, "Presione cualquier tecla para continuar...");
    _getch();
}

void Personajes(uint x, uint y, std::string modelo) {
    printxy(x, y, modelo);
    printxy(x + 5, y, "<- Jugador 1");
    printxy(x + 5, y + 1, "<- Jugador 2");
    printxy(x + 5, y + 2, "<- Jugador 3");
    printxy(x + 5, y + 3, "<- Jugador 4");
}

void Creditos() {
    clear();
    printxy(5, 5, "Cr�ditos del juego:");
    printxy(5, 7, "Desarrollado por: Tu Nombre");
    printxy(5, 8, "Versi�n: 1.0");
    printxy(5, 10, "Presione cualquier tecla para salir...");
    _getch();
}

#endif // PANTALLAS_H

