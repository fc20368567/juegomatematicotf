#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <conio.h>
#include "upc.h"

using namespace std;

using uint = unsigned int;
const uint MAX_COLUMNAS = 120;
const uint MAX_FILAS = 28;

const uint HEIGHT = 28;
const uint WIDTH = 120;

const string TL = "\xc9";
const string TR = "\xbb";
const string BL = "\xc8";
const string BR = "\xbc";
const string HO = "\xcd";
const string VE = "\xba";


void setForegroundColor(int color) {
    foreground(color);
}

void resetColor() {
    clearColor();
}
void printxy(uint x, int y, string txt) {
    gotoxy(x, y);
    cout << txt;
}

void gotoxyprint(int x, int y, string line) {
    gotoxy(x, y);
    cout << line;
}

void printFrame(uint x, uint y, uint width, uint height) {
    printxy(x, y, TL);
    printxy(x + width - 1, y, TR);
    printxy(x, y + height - 1, BL);
    printxy(x + width - 1, y + height - 1, BR);
    for (int i = 1; i < width - 1; ++i) {
        printxy(x + i, y, HO);
        printxy(x + i, y + height - 1, HO);
    }
    for (int i = 1; i < height - 1; ++i) {
        printxy(x, y + i, VE);
        printxy(x + width - 1, y + i, VE);
    }
}

void drawMiniMap(uint x, uint y, uint width, uint height) {
    printFrame(x, y, width, height);
    for (int i = 0; i < width * height / 10; ++i) {
        int mapX = x + 1 + rand() % (width - 2);
        int mapY = y + 1 + rand() % (height - 2);
        gotoxyprint(mapX, mapY, "*");
    }
}

void draw(const char screen[HEIGHT][WIDTH]) {
    for (int y = 0; y < HEIGHT; ++y) {
        gotoxy(1, y + 1);
        for (int x = 0; x < WIDTH; ++x) {
            cout << screen[y][x];
        }
    }
}

void generateSpaceMap(char screen[HEIGHT][WIDTH]) {
    for (int y = 0; y < HEIGHT; ++y) {
        for (int x = 0; x < WIDTH; ++x) {
            screen[y][x] = ' ';
        }
    }
    for (int i = 0; i < WIDTH * HEIGHT / 10; ++i) {
        int x = rand() % WIDTH;
        int y = rand() % HEIGHT;
        screen[y][x] = '*';
    }
}

void mostrarIntroduccion() {
    clear();
    printFrame(15, 8, 85, 15);
    printxy(19, 10, " __  __      _      _____   _   _   ");
    printxy(19, 11, "|  \\/  |    / \\    |_   _| | | | |  ");
    printxy(19, 12, "| |\\/| |   / _ \\     | |   | |_| |  ");
    printxy(19, 13, "| |  | |  / ___ \\    | |   |  _  |  ");
    printxy(19, 14, "|_|  |_| /_/   \\_\\   |_|   |_| |_|  ");
    printxy(19, 15, " ____    _   _    ___   __        __  _____   ____    ____  ");
    printxy(19, 16, "/ ___|  | | | |  / _ \\  \\ \\      / / | ____| |  _ \\  / ___| ");
    printxy(19, 17, "\\___ \\  | |_| | | | | |  \\ \\ /\\ / /  |  _|   | |_) | \\___ \\ ");
    printxy(19, 18, " ___) | |  _  | | |_| |   \\ V  V /   | |___  |  _ <   ___) |");
    printxy(19, 19, "|____/  |_| |_|  \\___/     \\_/\\_/    |_____| |_| \\_\\ |____/ ");
    while (_getch() != ' ');
}

int menuPrincipal(uint left, uint top, uint width, uint height) {
    int op;

    do {
        clear();
        printFrame(15, 8, 80, 15);
        printxy(42, 10, "   MENU PRINCIPAL");
        printxy(42, 11, "   ==============");
        printxy(42, 13, "1. Jugar");
        printxy(42, 14, "2. Instrucciones del juego");
        printxy(42, 15, "3. Personajes");
        printxy(42, 16, "4. Creditos");
        printxy(42, 17, "5. Salir");
        printxy(42, 19, "Escoja una opcion: ");
        cin >> op;
    } while (op < 1 || op > 5);

    return op;
}

void reglasDelJuego() {
    clear();
    printFrame(15, 8, 85, 15);
    printxy(16, 9, "Instrucciones del juego:");
    printxy(16, 11, "El astronauta debe resolver ejercicios de multiplicaci�n y divisi�n para disparar ");
    printxy(16, 12, "balas y destruir meteoritos. La calificaci�n se basa en el tiempo de resoluci�n,");
    printxy(16, 13, "otorgando hasta 3 estrellas. Los jugadores comienzan con 3 vidas (corazones), ");
    printxy(16, 14, "perdiendo una vida por cada ejercicio incorrecto. A medida que avanzan niveles y ");
    printxy(16, 15, "aumenta la dificultad, se requerir�n m�s vidas. Adem�s, habr� un modo extra ");
    printxy(16, 16, "con sumas y restas para ganar corazones extras al recolectar comida, fomentando ");
    printxy(16, 17, "un aprendizaje continuo y adaptativo.");
    printxy(16, 19, "Presiona una tecla para volver al men� principal...");
    _getch();
}

void jugar() {
    srand(static_cast<unsigned>(time(nullptr)));
    char screen[HEIGHT][WIDTH];

    const uint menuWidth = 40;
    const uint menuHeight = 15;
    const uint menuX = (MAX_COLUMNAS - menuWidth) / 2;
    const uint menuY = (MAX_FILAS - menuHeight) / 2;

    const uint mapWidth = 30;
    const uint mapHeight = 8;
    const uint mapX = menuX + 5;
    const uint mapY = menuY + 3;

    clear();
    printFrame(menuX, menuY, menuWidth, menuHeight);
    gotoxyprint(menuX + 2, menuY + 2, "Mapa: El espacio");
    drawMiniMap(mapX, mapY, mapWidth, mapHeight);
    gotoxyprint(menuX + 2, menuY + mapHeight + 4, "Pulsa \"1\" para entrar al mapa");

    while (true) {
        if (_kbhit()) {
            char ch = _getch();
            if (ch == '1') {
                break;
            }
        }
    }

    generateSpaceMap(screen);

    clear();
    draw(screen);

    int playerX = WIDTH / 2;
    int playerY = HEIGHT - 5;
    const char* playerModel[] = {
        "   l'''''''l   ",
        "   l     lO  Ol",
        " l''l     '''''",
        "l   l         l",
        "L...l         l",
        "   l...l''l...l"
    };
    const int playerModelHeight = 6;
    const int playerModelWidth = 13;

    auto drawPlayer = [&](int x, int y) {
        for (int i = 0; i < playerModelHeight; ++i) {
            gotoxyprint(x, y + i, playerModel[i]);
        }
        };

    auto clearPlayer = [&](int x, int y) {
        for (int i = 0; i < playerModelHeight; ++i) {
            gotoxyprint(x, y + i, string(playerModelWidth, ' '));
        }
        };

    clear();
    draw(screen);
    drawPlayer(playerX, playerY);

    while (true) {
        if (_kbhit()) {
            char ch = _getch();
            clearPlayer(playerX, playerY);
            switch (ch) {
            case 'w':
                if (playerY > 1) playerY--;
                break;
            case 's':
                if (playerY < HEIGHT - playerModelHeight - 1) playerY++;
                break;
            case 'a':
                if (playerX > 1) playerX--;
                break;
            case 'd':
                if (playerX < WIDTH - playerModelWidth) playerX++;
                break;
            case 'q':
                return;
            default:
                break;
            }
            clear();
            draw(screen);
            drawPlayer(playerX, playerY);
        }
    }
}


void Personajes(uint x, uint y, string modelo) {
    clear();
    static int index = 0;
    static const string modelos[] = { "HEROE", "VILLANO (METEORITO)", "TUTOR" };
    const int numModelos = sizeof(modelos) / sizeof(modelos[0]);

    setForegroundColor(DARK_BLUE);
    printFrame(15, 8, 85, 15);
    setForegroundColor(DARK_BLUE);
    printxy(16, 9, "Instrucciones del juego:");
    resetColor();

    if (index < numModelos) {
        if (modelo == modelos[index]) {
            if (modelo == "HEROE") {
                color(BRIGHT_GREEN);
            }
            else if (modelo == "VILLANO (METEORITO") {
                color(BRIGHT_RED);
            }
            else if (modelo == "TUTOR") {
                color(BRIGHT_BLUE);
            }


            printxy(26, 10, modelo);
            if (modelo == "HEROE") {
                printxy(16, 11, "   l'''''''l   ");
                printxy(16, 12, "   l     lO  Ol");
                printxy(16, 13, " l''l     '''''");
                printxy(16, 14, "l   l         l");
                printxy(16, 15, "L...l         l");
                printxy(16, 16, "   l...l''l...l");
            }
            else if (modelo == "VILLANO (METEORITO)") {
                printxy(16, 11, "      l _   _ l      ");
                printxy(16, 12, "     l' O'''O 'l     ");
                printxy(16, 13, "      l  '''  l      ");
                printxy(16, 14, "    l           l    ");
                printxy(16, 15, "      l       l      ");
                printxy(16, 16, "    l...l'''l...l    ");
            }
            else if (modelo == "TUTOR") {
                printxy(16, 11, "     l'''''''l     ");
                printxy(16, 12, "  |''l'''''''''l''l ");
                printxy(16, 13, "  '''l  O''''O  l''' ");
                printxy(16, 14, "      l        l    ");
                printxy(16, 15, "     l          l   ");
                printxy(16, 16, "     l...l''l...l   ");
            }
        }
        index++;
    }
    else {
        index = 0;
    }

    resetColor();
    printxy(16, 18, "Presiona una tecla para volver al menu principal...");
    _getch();
}

void Creditos() {
    clear();

    int redColor = BRIGHT_RED;

    color(redColor);
    printxy(30, 10, "                  **                    ");
    printxy(30, 11, "               ****                    ");
    printxy(30, 12, "      **      *****            **      ");
    printxy(30, 13, "    **       ******              **    ");
    printxy(30, 14, "   **        ********             **   ");
    printxy(30, 15, "  ***        **********           ***  ");
    printxy(30, 16, " ***#        ***********#         #*** ");
    printxy(30, 17, "****         ************#         ****");
    printxy(30, 18, "*****         ************        *****");
    printxy(30, 19, "*****           **********        *****");
    printxy(30, 20, "******            ********       ******");
    printxy(30, 21, "*******            *******      *******");
    printxy(30, 22, "********            *****      ********");
    printxy(30, 23, " **********         ****    ********** ");
    printxy(30, 24, "  ************      ***  ************  ");
    printxy(30, 25, "   *********************************   ");
    printxy(30, 26, "    *******************************    ");
    printxy(30, 27, "      ***************************      ");
    printxy(30, 28, "        ***********************        ");
    printxy(30, 29, "            ***************             ");

    color(WHITE);

    cout << endl << endl << "Integrantes:" << endl;
    cout << "- Breithner Rodolfo Perez Encarnacion" << endl;
    cout << "- Bracque Rodrigo Osorio Niquin" << endl;
    cout << "- Fernando Fabrizio Contreras Panuera" << endl << endl;
    cout << "Presiona la tecla ESPACIO para continuar...";
    while (_getch() != ' ');
    clearColor();
}


int main() {
    mostrarIntroduccion();
    uint width = 40;
    uint height = 12;
    uint left = MAX_COLUMNAS / 2 - width / 2;
    uint top = MAX_FILAS / 2 - height / 2;

    int opcion;

    do {
        opcion = menuPrincipal(left, top, width, height);
        switch (opcion) {
        case 1:
            jugar();
            break;
        case 2:
            reglasDelJuego();
            break;
        case 3:
            for (const string& modelo : { "HEROE", "VILLANO (METEORITO)", "TUTOR" }) {
                Personajes(left, top, modelo);
            }
            break;
        case 4:
            Creditos();
            break;
        }
    } while (opcion != 5);

    return EXIT_SUCCESS;
}

